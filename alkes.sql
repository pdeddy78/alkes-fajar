-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 25, 2015 at 06:24 AM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `alkes`
--

-- --------------------------------------------------------

--
-- Table structure for table `tm_alkes`
--

CREATE TABLE IF NOT EXISTS `tm_alkes` (
  `alkes_id` int(11) NOT NULL,
  `alkes_name` varchar(200) DEFAULT NULL,
  `alkes_price` varchar(200) DEFAULT NULL,
  `alkes_desc` text,
  `alkes_img` varchar(200) DEFAULT NULL,
  `alkes_status` char(1) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tm_alkes`
--

INSERT INTO `tm_alkes` (`alkes_id`, `alkes_name`, `alkes_price`, `alkes_desc`, `alkes_img`, `alkes_status`) VALUES
(1, 'Nesco Multicheck', '550000', 'Nesco Multicheck', '1.png', '1'),
(2, 'Accu Check Active', '180000', 'Accu Check Active', '2.png', '1'),
(3, 'Multi Check Cholesterol', '130000', 'Multi Check Cholesterol', '3.png', '1'),
(4, 'Vinmed Digital Thermometer', '18000', 'Vinmed Digital Thermometer', '4.png', '1'),
(5, 'Digital Blood Pressure Monitor', '250000', 'Digital Blood Pressure Monitor', '5.png', '1'),
(6, 'Nelco Multicheck Uric Acid', '65000', 'Nelco Multicheck Uric Acid', '6.png', '1'),
(7, 'Polygreen Blood Pressure Monitor', '275000', 'Polygreen Blood Pressure Monitor', '7.png', '1'),
(8, 'Vin Med Alcohol Pad', '15000', 'Vin Med Alcohol Pad', 'no_pict.png', '1'),
(9, 'Kursi Roda', '850000', 'Kursi Roda', 'no_pict.png', '1'),
(10, 'Stethoscope', '40000', 'Stethoscope', 'no_pict.png', '1'),
(11, 'Tes Kehamilan Instan', '60000', 'Tes Kehamilan Instan', 'no_pict.png', '1'),
(12, 'Medical Latex Examination Glove', '38000', 'Medical Latex Examination Glove', 'no_pict.png', '1'),
(13, 'Sringe One Med 1ml', '65000', 'Sringe One Med 1ml', 'no_pict.png', '1'),
(14, 'Inflo 22G', '4000', 'Inflo 22G', 'no_pict.png', '1'),
(15, 'Vin Med Tomy Quit', '20000', 'Vin Med Tomy Quit', 'no_pict.png', '1'),
(16, 'Ultrasound Transmission Gell', '20000', 'Ultrasound Transmission Gell', 'no_pict.png', '1'),
(17, 'Otoscope', '350000', 'Otoscope', 'no_pict.png', '1'),
(18, 'Tensi Aneroid', '65000', 'Tensi Aneroid', 'no_pict.png', '1'),
(19, 'Statur Meter', '45000', 'Statur Meter', 'no_pict.png', '1'),
(20, 'Masker Tali', '20000', 'Masker Tali', 'no_pict.png', '1'),
(21, 'B-Jes Diagnostick', '35000', 'B-Jes Diagnostick', 'no_pict.png', '1'),
(22, 'Cut Gut Cromic', '7000', 'Cut Gut Cromic', 'no_pict.png', '1'),
(23, 'Timbangan Gantung', '60000', 'Timbangan Gantung', 'no_pict.png', '1'),
(24, 'Celana Hernia', '20000', 'Celana Hernia', 'no_pict.png', '1'),
(25, 'Infusion', '5000', 'Infusion', 'no_pict.png', '1'),
(26, 'Thermometer Ruangan', '20000', 'Thermometer Ruangan', 'no_pict.png', '1'),
(27, 'Cut Gut Plain', '12000', 'Cut Gut Plain', 'no_pict.png', '1'),
(28, 'Meja Periksa', '1200000', 'Meja Periksa', 'no_pict.png', '1'),
(29, 'Kapas Pembersih Serbaguna', '25000', 'Kapas Pembersih Serbaguna', 'no_pict.png', '1'),
(30, 'Sarung Tangan Gynecology', '15000', 'Sarung Tangan Gynecology', 'no_pict.png', '1'),
(31, 'Tabung O2', '700000', 'Tabung O2', 'no_pict.png', '1'),
(32, 'Celana Sunat', '15000', 'Celana Sunat', 'no_pict.png', '1'),
(33, 'Tiang Infus', '80000', 'Tiang Infus', 'no_pict.png', '1'),
(34, 'Needle', '30000', 'Needle', 'no_pict.png', '1'),
(35, 'Tongkat Ketuk', '130000', 'Tongkat Ketuk', 'no_pict.png', '1'),
(36, 'Cut Gut Plain 3.0', '5000', 'Cut Gut Plain 3.0', 'no_pict.png', '1'),
(37, 'Blood Uric Acid', '65000', 'Blood Uric Acid', 'no_pict.png', '1'),
(38, 'Blood Glucosa', '65000', 'Blood Glucosa', 'no_pict.png', '1'),
(39, 'Vin Med Tensi Digital', '450000', 'Vin Med Tensi Digital', 'no_pict.png', '1'),
(40, 'Sringe One Med 3 ml', '60000', 'Sringe One Med 3 ml', 'no_pict.png', '1'),
(41, 'Alkohol 70%', '3000', 'Alkohol 70%', 'no_pict.png', '1'),
(42, 'Cairan RL', '5500', 'Cairan RL', 'no_pict.png', '1'),
(43, 'Timbangan Camry', '65000', 'Timbangan Camry', 'no_pict.png', '1'),
(44, 'Ultrasonic Transmission Gel', '750000', 'Ultrasonic Transmission Gel', 'no_pict.png', '1'),
(45, 'Tensi GEA', '180000', 'Tensi GEA', 'no_pict.png', '1'),
(46, 'Nebolizer Omron', '750000', 'Nebolizer Omron', 'no_pict.png', '1'),
(47, 'Kassa Steril', '20000', 'Kassa Steril', 'no_pict.png', '1'),
(48, 'Tensi GCHB E/T', '550000', 'Tensi GCHB E/T', 'no_pict.png', '1'),
(49, 'Timbangan Bayi', '250000', 'Timbangan Bayi', 'no_pict.png', '1'),
(50, 'Oxigen Regulator', '200000', 'Oxigen Regulator', 'no_pict.png', '1'),
(51, 'Fettal Doppler', '550000', 'Fettal Doppler', 'no_pict.png', '1'),
(52, 'Cut Gut Brown', '7000', 'Cut Gut Brown', 'no_pict.png', '1'),
(53, 'Poly Cheter', '9000', 'Poly Cheter', 'no_pict.png', '1'),
(54, 'Philips Senter Terapi', '750000', 'Philips Senter Terapi', 'no_pict.png', '1'),
(55, 'Sarung Tangan Giluex', '8000', 'Sarung Tangan Giluex', 'no_pict.png', '1'),
(56, 'Syringe 3ml', '85000', 'Syringe 3ml', 'no_pict.png', '1'),
(57, 'Sensi Pads', '20000', 'Sensi Pads', 'no_pict.png', '1'),
(58, 'Kapas 250 & 500 gr', '25000', 'Kapas 250 & 500 gr', 'no_pict.png', '1'),
(59, 'Exyglove', '38000', 'Exyglove', 'no_pict.png', '1'),
(60, 'Kotak P3K Jinjing', '25000', 'Kotak P3K Jinjing', 'no_pict.png', '1'),
(61, 'Kotak P3K', '60000', 'Kotak P3K', 'no_pict.png', '1'),
(62, 'Kassa Gulung', '120000', 'Kassa Gulung', 'no_pict.png', '1'),
(63, 'Extractor Mucus', '12000', 'Extractor Mucus', 'no_pict.png', '1'),
(64, 'Terapi Badan Electric', '40000', 'Terapi Badan Electric', 'no_pict.png', '1'),
(65, 'Penghangat Badan', '35000', 'Penghangat Badan', 'no_pict.png', '1'),
(66, 'Nelco Multi Check Glucose', '75000', 'Nelco Multi Check Glucose', 'no_pict.png', '1'),
(67, 'Kain Timbangan Bayi', '20000', 'Kain Timbangan Bayi', 'no_pict.png', '1'),
(68, 'Celemek Plastik', '10000', 'Celemek Plastik', 'no_pict.png', '1'),
(69, 'General Care', '20000', 'General Care', 'no_pict.png', '1'),
(70, 'Tongkat Kaki 4', '100000', 'Tongkat Kaki 4', '70.png', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tm_user`
--

CREATE TABLE IF NOT EXISTS `tm_user` (
  `user_id` int(11) NOT NULL,
  `user_email` varchar(200) DEFAULT NULL,
  `user_password` varchar(50) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tm_user`
--

INSERT INTO `tm_user` (`user_id`, `user_email`, `user_password`) VALUES
(1, 'fajar@fajar-isnandio.com', '7594c841617c1e7a9d6c0814c16512f85146b2b4');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tm_alkes`
--
ALTER TABLE `tm_alkes`
  ADD PRIMARY KEY (`alkes_id`);

--
-- Indexes for table `tm_user`
--
ALTER TABLE `tm_user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tm_alkes`
--
ALTER TABLE `tm_alkes`
  MODIFY `alkes_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=71;
--
-- AUTO_INCREMENT for table `tm_user`
--
ALTER TABLE `tm_user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
