<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Webadmin_model extends CI_Model {
    function __construct()
    {
        parent::__construct();
    }
    
    /*==================================== ssp_tm_alkes ============================================*/
    function ssp_tm_alkes($aColumns, $sWhere, $sOrder, $sLimit)
    {
        $query = $this->db->query("
           SELECT * FROM (
                SELECT a.*, CONCAT_WS('|', a.alkes_id) AS add_data
                FROM tm_alkes a
            ) A
            $sWhere
            $sOrder
            $sLimit
        ");
        
        return $query;
    }
    
    function ssp_tm_alkes_total($sIndexColumn){
        $query = $this->db->query("
            SELECT $sIndexColumn
            FROM (
                SELECT a.*, CONCAT_WS('|', a.alkes_id) AS add_data
                FROM tm_alkes a
            ) A
        ");
        
        return $query;
    }
    /*==================================== end of ssp_tm_alkes =====================================*/
    
    function tm_alkes_total(){
        return $this->db->count_all_results('tm_alkes');
    }
    
    function tm_alkes_insert($in){
        return $this->db->insert('tm_alkes', $in);
    }
    
    function get_row_tm_alkes($id){
        $array = array(
            'alkes_id' => $id
        );
        $this->db->where($array);
        $this->db->from('tm_alkes');
        
        return $this->db->get();
    }
    
    function tm_alkes_update($in){
        return $this->db->update('tm_alkes', $in, array('alkes_id' => $in['alkes_id']));
    }
}