<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login_model extends CI_Model {
    function __construct()
    {
        parent::__construct();
    }
    
    // check login
    function check_login($data){
        $array = array(
            'user_email' => $data['user_email'],
            'user_password' => $data['user_password']
        );
        $this->db->where($array);
        $this->db->from('tm_user');
        return $this->db->get();
    }
}