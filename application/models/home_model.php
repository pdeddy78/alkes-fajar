<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home_model extends CI_Model {
    function __construct()
    {
        parent::__construct();
    }
    
    // get tm_alkes
    function get_tm_alkes(){
        // active record select
        $this->db->where('alkes_status', '1');
        $this->db->from('tm_alkes');
        return $this->db->get();
    }
}