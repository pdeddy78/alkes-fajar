<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Webadmin extends CI_Controller {
    function __construct()
    {
        parent::__construct();
        
        // login check
        if(!$this->session->userdata('logged_in')){
            redirect('login');
        }
        
        $this->load->model('webadmin_model');
    }
    
	public function index()
	{
	    $data = array(
	       'content' => 'backend/includes/content'
        );
		$this->load->view('backend/template', $data);
	}
    
    function alkes(){
        $data = array(
           'content' => 'backend/alkes'
        );
        $this->load->view('backend/template', $data);
    }
    
    /*======================================= ssp_tm_alkes ===================================*/
    function ssp_tm_alkes(){
        $aColumns = array('alkes_id', 'alkes_name', 'alkes_price', 'alkes_status', 'add_data');
        
        $sIndexColumn = "alkes_id";
        
        // paging
        $sLimit = "";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' ){
            $sLimit = "LIMIT ".mysql_real_escape_string( $_GET['iDisplayStart'] ).", ".
                mysql_real_escape_string( $_GET['iDisplayLength'] );
        }
        $numbering = mysql_real_escape_string( $_GET['iDisplayStart'] );
        $page = 1;
        
        // ordering
        if ( isset( $_GET['iSortCol_0'] ) ){
            $sOrder = "ORDER BY  ";
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ ){
                if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" ){
                    $sOrder .= $aColumns[ intval( $_GET['iSortCol_'.$i] ) ]."
                        ".mysql_real_escape_string( $_GET['sSortDir_'.$i] ) .", ";
                }
            }
            
            $sOrder = substr_replace( $sOrder, "", -2 );
            if ( $sOrder == "ORDER BY" ){
                $sOrder = "";
            }
        }

        // filtering
        $sWhere = "";
        if ( $_GET['sSearch'] != "" ){
            $sWhere = "WHERE (";
            for ( $i=0 ; $i<count($aColumns) ; $i++ ){
                $sWhere .= $aColumns[$i]." LIKE '%".mysql_real_escape_string( $_GET['sSearch'] )."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }
        
        // individual column filtering
        for ( $i=0 ; $i<count($aColumns) ; $i++ ){
            if ( $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' ){
                if ( $sWhere == "" ){
                    $sWhere = "WHERE ";
                }
                else{
                    $sWhere .= " AND ";
                }
                $sWhere .= $aColumns[$i]." LIKE '%".mysql_real_escape_string($_GET['sSearch_'.$i])."%' ";
            }
        }
        
        $rResult = $this->webadmin_model->ssp_tm_alkes($aColumns, $sWhere, $sOrder, $sLimit);
        
        $iFilteredTotal = 10;
        
        $rResultTotal = $this->webadmin_model->ssp_tm_alkes_total($sIndexColumn);
        $iTotal = $rResultTotal->num_rows();
        $iFilteredTotal = $iTotal;
        
        $output = array(
            "sEcho" => intval($_GET['sEcho']),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iFilteredTotal,
            "aaData" => array()
        );
        
        foreach ($rResult->result_array() as $aRow){
            $row = array();
            for ( $i=0 ; $i<count($aColumns) ; $i++ ){
                /* General output */
                if($i < 1)
                    $row[] = $numbering+$page.'|'.$aRow[ $aColumns[$i] ];
                else
                    $row[] = $aRow[ $aColumns[$i] ];
            }
            $page++;
            $output['aaData'][] = $row;
        }
        
        echo json_encode( $output );
    }
    /*======================================= end of ssp_tm_alkes ===================================*/
    
    function add_alkes(){
        if(isset($this->error_massage)){
            $error = $this->error_massage;
        }
        else {
            $error = '';
        }
        
        $data = array(
           'content' => 'backend/add_alkes',
           'error' => $error
        );
        $this->load->view('backend/template', $data);
    }
    
    function tm_alkes_insert(){
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('alkes_name', 'Name', 'required');
        $this->form_validation->set_rules('alkes_desc', 'Description', 'required');
        $this->form_validation->set_rules('alkes_price', 'Price', 'required');
        
        if ($this->form_validation->run() == FALSE){
            $this->add_alkes();
        }
        else{
            $total = $this->webadmin_model->tm_alkes_total();
            $id = $total + 1;
            
            $config = array(
                'file_name' => ''.$id.'',
                'upload_path' => './images/item/',
                'allowed_types' => 'png',
                'max_size' => '200',
                'max_width' => '320',
                'max_height' => '150',
                'overwrite' => TRUE
            );
            $this->load->library('upload', $config);
            
            if (!$this->upload->do_upload('alkes_img')){
                $error = $this->upload->display_errors('','');
                
                if($error == 'You did not select a file to upload.'){
                    $in = array(
                        'alkes_name' => $this->input->post('alkes_name'),
                        'alkes_price' => $this->input->post('alkes_price'),
                        'alkes_desc' => $this->input->post('alkes_desc'),
                        'alkes_img' => 'no_pict.png',
                        'alkes_status' => $this->input->post('alkes_status')
                    );
                    if($this->webadmin_model->tm_alkes_insert($in)){
                        redirect('webadmin/alkes');
                    }
                }
                else{
                    $this->error_massage = $error;
                    $this->add_alkes();
                }
            }
            else{
                $data = $this->upload->data();
                $in = array(
                    'alkes_name' => $this->input->post('alkes_name'),
                    'alkes_price' => $this->input->post('alkes_price'),
                    'alkes_desc' => $this->input->post('alkes_desc'),
                    'alkes_img' => $data['file_name'],
                    'alkes_status' => $this->input->post('alkes_status')
                );
                if($this->webadmin_model->tm_alkes_insert($in)){
                    redirect('webadmin/alkes');
                }
            }
        }
    }

    function edit_alkes(){
        if(isset($this->error_massage)){
            $error = $this->error_massage;
        }
        else {
            $error = '';
        }
        
        $id = $this->uri->segment(3, '');
        
        $data = array(
           'content' => 'backend/edit_alkes',
           'error' => $error,
           'data_row' => $this->webadmin_model->get_row_tm_alkes($id)->row()
        );
        $this->load->view('backend/template', $data);
    }
    
    function tm_alkes_update(){
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('alkes_name', 'Name', 'required');
        $this->form_validation->set_rules('alkes_desc', 'Description', 'required');
        $this->form_validation->set_rules('alkes_price', 'Price', 'required');
        
        if ($this->form_validation->run() == FALSE){
            $this->add_alkes();
        }
        else{
            $id = $this->input->post('alkes_id');
            
            $config = array(
                'file_name' => ''.$id.'',
                'upload_path' => './images/item/',
                'allowed_types' => 'png',
                'max_size' => '200',
                'max_width' => '320',
                'max_height' => '150',
                'overwrite' => TRUE
            );
            $this->load->library('upload', $config);
            
            if (!$this->upload->do_upload('alkes_img')){
                $error = $this->upload->display_errors('','');
                
                if($error == 'You did not select a file to upload.'){
                    $in = array(
                        'alkes_id' => $id,
                        'alkes_name' => $this->input->post('alkes_name'),
                        'alkes_price' => $this->input->post('alkes_price'),
                        'alkes_desc' => $this->input->post('alkes_desc'),
                        'alkes_img' => $this->input->post('file_name'),
                        'alkes_status' => $this->input->post('alkes_status')
                    );
                    if($this->webadmin_model->tm_alkes_update($in)){
                        redirect('webadmin/alkes');
                    }
                }
                else{
                    $this->error_massage = $error;
                    $this->add_alkes();
                }
            }
            else{
                $data = $this->upload->data();
                $in = array(
                    'alkes_id' => $id,
                    'alkes_name' => $this->input->post('alkes_name'),
                    'alkes_price' => $this->input->post('alkes_price'),
                    'alkes_desc' => $this->input->post('alkes_desc'),
                    'alkes_img' => $data['file_name'],
                    'alkes_status' => $this->input->post('alkes_status')
                );
                if($this->webadmin_model->tm_alkes_update($in)){
                    redirect('webadmin/alkes');
                }
            }
        }
    }
    
    // Log Out
    function logout(){
        $this->session->sess_destroy();
        redirect('login');
    }
}