<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
    function __construct()
    {
        parent::__construct();
        
        $this->load->model('home_model');
    }
    
	public function index()
	{
        $data = array(
            'data_alkes' => $this->home_model->get_tm_alkes()
        );
		$this->load->view('frontend/template', $data);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */