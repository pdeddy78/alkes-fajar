<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {
    function __construct(){
        parent::__construct();
        // check login
        if($this->session->userdata('logged_in'))
        {
            redirect('webadmin');
        }
        
        $this->load->model('login_model');
    }
    
	public function index(){
	    if(isset($this->error_massage)){
            $data = array(
               'message' => $this->error_massage
            );
        }
        else {
            $data = array(
               'message' => ''
            );
        }
		$this->load->view('backend/login', $data);
	}
    
    function login_check(){
        $this->load->library('encrypt');
        
        $data = array(
            'user_email' => $this->input->post('email'),
            'user_password' => $this->encrypt->sha1($this->input->post('password'))
        );
        $query = $this->login_model->check_login($data);
        if($query->num_rows() > 0){
            $row = $query->row();
            $data = array(
                'user_id' => $row->user_id,
                'user_email' => $row->user_email,
                'logged_in' => TRUE
            );  
            $this->session->set_userdata($data);
            redirect('webadmin');
        }
        else{
            $this->error_massage = 'Wrong email or password!';
            $this->index();
        }
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */