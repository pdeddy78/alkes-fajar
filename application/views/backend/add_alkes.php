<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Add New</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <?php echo form_open_multipart('webadmin/tm_alkes_insert', array('role' => 'form', 'class' => 'form-horizontal'));?>
                    <div class="form-group">
                        <div class="col-md-8">
                            <label>Name</label>
                            <input type="text" class="form-control" name="alkes_name" id="alkes_name" maxlength="200" placeholder="Name" value="<?=set_value('alkes_name')?>">   
                        </div>
                    </div>
                    <?=form_error('alkes_name', '<div class="alert alert-danger alert-dismissible" role="alert">', '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button></div>'); ?>
                    <div class="form-group">
                        <div class="col-md-6">
                            <label>Description</label>
                            <textarea class="form-control" name="alkes_desc" id="alkes_desc" rows="2" placeholder="Description"><?=set_value('alkes_desc')?></textarea> 
                        </div>
                    </div>
                    <?=form_error('alkes_desc', '<div class="alert alert-danger alert-dismissible" role="alert">', '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button></div>'); ?>
                    <div class="form-group">
                        <div class="col-md-3">
                            <label>Price</label>
                            <input type="text" class="form-control price" id="alkes_price" placeholder="Price" value="<?=set_value('alkes_price')?>">
                            <input type="hidden" name="alkes_price" value="<?=set_value('alkes_price')?>" />   
                        </div>
                    </div>
                    <?=form_error('alkes_price', '<div class="alert alert-danger alert-dismissible" role="alert">', '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button></div>'); ?>
                    <div class="form-group">
                        <div class="col-md-5">
                            <label>Image</label>
                            <div class="well">
                                <center>
                                    <img id="thumb_image" src="<?=base_url()?>images/item/no_pict.png" /><br /><br />
                                    <span id="thumb_delete" class="glyphicon glyphicon-trash" style="cursor: pointer;">
                                </center>
                            </div>
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <input rel="tooltip" title="Browse File" class="btn btn-primary" type="button" value="Browse ..." onclick="$(this).parent().find('input[type=file]').click();">
                                    <input type="file" style="visibility:hidden; width: 1px; height: 1px;" id="alkes_img" name="alkes_img" onchange="validate_file(this)">
                                </span>
                                <input type="text" class="form-control normal-readonly text_file" placeholder="No file chosen" readonly>
                            </div>
                            <p class="help-block">only small images (max. 320 x 150 and max. size 200KB)</p>   
                        </div>
                    </div>
                    <?php
                    if($error != ''){
                        ?>
                    <div class="alert alert-danger alert-dismissible" role="alert"><?=$error?><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button></div>    
                        <?php
                    }
                    ?>
                    <div class="form-group">
                        <div class="col-md-3">
                            <label>Status</label>
                            <select class="form-control" name="alkes_status" id="alkes_status">
                                <option value="1">Active</option>
                                <option value="0">Non Active</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-3">
                            <div class="btn-group">
                                <button type="submit" class="btn btn-primary">Save</button>
                                <a class="btn btn-danger" href="<?=base_url()?>webadmin/alkes">Cancel</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<script>
    function validate_file(obj){
        var file_name = $(obj).val().replace('C:\\fakepath\\', '');
        var file_name_attr = file_name.split('.');
        file_name_attr[2] = obj.files[0].size/1024;
        
        if(file_name_attr[1] != 'png' || file_name_attr[2] > 200){
            $(obj).wrap('<form>').closest('form').get(0).reset();
            $(obj).unwrap();
            $(obj).parent().parent().find('.text_file').val('');
            readURL(obj, 'set');
            alert('File must png and maximum file size under 200 KB!');
        }
        else{
            $(obj).parent().parent().find('.text_file').val(file_name);
            $('#thumb_delete').fadeIn();
            readURL(obj);
        }
    }
    
    function readURL(input, type) {
        if (type != 'set'){
            if (input.files && input.files[0]) {
                var reader = new FileReader();
        
                reader.onload = function (e) {
                    $('#thumb_image').attr('src', e.target.result);
                }
        
                reader.readAsDataURL(input.files[0]);
            }
        }
        else{
            $('#thumb_image').attr('src', baseurl + 'images/item/no_pict.png');
        }
    }
    
    $(function(){
        $('#thumb_delete').fadeOut();
        
        $('#thumb_delete').click(function(){
            $('#thumb_image').attr('src', baseurl+'images/item/no_pict.png');
            var obj = $('#alkes_img');
            
            obj.wrap('<form>').closest('form').get(0).reset();
            obj.unwrap();
            obj.parent().parent().find('.text_file').val('');
            $(this).fadeOut();
        });
        
        $('#alkes_price').change(function(){
            var value = $(this).autoNumeric('get');
            $(this).parent().find('input[type="hidden"]').val(value);
        });
    });
</script>