<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">List Alkes</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-list">
                        <thead>
                            <tr>
                                <th class="title-center">No.</th>
                                <th class="title-center">Name</th>
                                <th class="title-center">Price</th>
                                <th class="title-center">Status</th>
                                <th class="title-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<script>
    $(function() {
        $('#dataTables-list').dataTable({
            "fnCreatedRow": function( nRow, aData, iDataIndex ) {
                var temp = $('td:eq(0)', nRow).text();
                var temp = temp.split('|');
                var numbering = temp[0];
                var id = temp[1];
                
                var price = $('td:eq(2)', nRow).text();
                
                var status = $('td:eq(3)', nRow).text();
                if(status == '1'){
                    status = 'Active';
                }
                else{
                    status = 'Non Active';
                }
                
                var string = '<a href="'+baseurl+'webadmin/edit_alkes/'+id+'"><span class="glyphicon glyphicon-edit"></span></a>';
                
                $('td:eq(0)', nRow).html(numbering);
                $('td:eq(2)', nRow).html('<span class="price">'+ price +'</span>');
                $('td:eq(3)', nRow).html(status);
                $('td:eq(4)', nRow).html(string);
                $('td:eq(0),td:eq(3),td:eq(4)', nRow).css('text-align','center');
                $('td:eq(2)', nRow).css('text-align','right');
                $('td:eq(2)', nRow).find('.price').autoNumeric('init', {aSep: '.', aDec: ',',  mDec: '0'});
            },
            "bAutoWidth": false, // Disable the auto width calculation 
            "aoColumns": [
                { "sWidth": "6%" },
                { "sWidth": "52%" },
                { "sWidth": "20%" },
                { "sWidth": "15%" },
                { "sWidth": "7%" }
            ],
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": baseurl+"webadmin/ssp_tm_alkes"
        });
        $('#dataTables-list').each(function(){
            var datatable = $(this);
            // LENGTH - Inline-Form control
            var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
            length_sel.addClass('form-control input-sm');
            length_sel.parent().parent().html('<a type="button" class="btn btn-default btn-sm" href="<?=base_url()?>webadmin/add_alkes"><span class="glyphicon glyphicon-plus"></span> Add New</a>');
        });
    });
</script>