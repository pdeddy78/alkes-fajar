<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Supplier Alat-Alat Kesehatan">
    <meta name="author" content="Fajar Isnandio Cindyka">
    <link rel="shortcut icon" href="<?=base_url()?>images/new_logo(50x50).png">

    <title>CV. CAHAYA ABADI</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?=base_url()?>css/bootstrap.min.css" rel="stylesheet">
    
    <!-- Custom CSS -->
    <link href="<?=base_url()?>css/shop-homepage.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        body{
            background: url('<?=base_url()?>/images/background.jpg');
        }
        
        .dark-grey{
            color: darkgrey !important;
        }
        .red{
            color: red !important;
        }
        .blue{
            color: blue !important;
        }
    </style>
</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <!-- <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button> -->
                <a class="navbar-brand" href="<?=base_url()?>" style="padding-top: 5px; padding-right: 5px;"><img src="<?=base_url()?>images/new_logo(500x500).png" style="max-height: 40px;" /></a>
                <a class="navbar-brand dark-grey" href="<?=base_url()?>">CV. <span class="red">CAHAYA</span> <span class="blue">ABADI</span></a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse pull-right" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="<?=base_url()?>webadmin">Webadmin</a>
                    </li>
                </ul>
            </div>
            <div class="navbar-text pull-right dark-grey hidden-xs">Supplier Alat - Alat Kesehatan</div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
    <!-- Page Content -->
    <div class="container">

        <div class="row">
            <div class="col-md-3">
                <p class="lead dark-grey">Office</p>
                <p class="dark-grey">
                    Jl. Bukit Baja Sejahtera III Block F4 NO. 11, Cilegon, Banten<br />
                </p>
                <!-- Start Google Map -->
                <div id="map" style="min-height: 200px;"></div>
                <!-- End -->
                <br /><br />
                <p class="lead dark-grey">Contact</p>
                <p class="dark-grey">
                    Marketing <br />
                    Abdul khalik : 0819 1123 1275 <br />Rohman : 0877 7189 9793<br /> 
                    <br />
                    Support<br />
                    Office : 0819 1117 8858<br />
                    E-mail : cv.cahaya_abadi12@yahoo.com
                </p>
            </div>

            <div class="col-md-9">

                <div class="row">
                    <?php
                    foreach ($data_alkes->result() as $row) {
                        ?>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                        <div class="thumbnail">
                            <img style="max-height: 122px;" src="<?=base_url()?>images/item/<?=$row->alkes_img?>" alt="">
                            <div class="caption">
                                <h5><?=$row->alkes_id?>. <?=$row->alkes_name?></h5>
                                <h5 class="pull-right">Rp. <span class="price"><?=$row->alkes_price?></span></h5><br /><br />
                                <p>Description :<br /> <?=$row->alkes_desc?></p>
                            </div>
                            <!-- <div class="ratings">
                                <p class="pull-right">15 reviews</p>
                                <p>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                </p>
                            </div> -->
                        </div>
                    </div>
                        <?php
                    }
                    ?>
                </div>

            </div>

        </div>

    </div>
    <!-- /.container -->

    <div class="container">

        <hr style="border-color: darkgrey;">

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p class="pull-right dark-grey">Copyright &copy; 2014 - Cahaya Abadi || Powered By <a href="https://fajar-isnandio.com">Fajar-Isnandio.com</a></p>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.container -->

    <!-- jQuery Version 1.11.0 -->
    <script src="<?=base_url()?>js/jquery-1.11.0.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?=base_url()?>js/bootstrap.min.js"></script>
    
    <!-- Start Google Map JavaScript -->
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
    <script type="text/javascript" src="<?=base_url()?>js/gmaps.js"></script>
    <script type="text/javascript">
        var map;
        $(document).ready(function(){
          
          // main directions
          map = new GMaps({
            el: '#map', lat: -6.019360, lng: 106.036075, zoom: 15, zoomControl : true, 
            zoomControlOpt: { style : 'SMALL', position: 'TOP_LEFT' }, panControl : false, scrollwheel: false
          });
          // add address markers
          map.addMarker({ lat: -6.019360, lng: 106.036075, title: 'Office',
          infoWindow: { content: '<p>Jl. Bukit Baja Sejahtera III Block F4 NO. 11, Cilegon, Banten</p>' } });
        });
    </script>
    <!-- End Google Map JavaScript -->
    
    <!-- jQuery niceScroll -->
    <script src="<?=base_url()?>js/jquery.nicescroll.js"></script>
    
    <!-- autoNumeric -->
    <script src="<?=base_url()?>js/autoNumeric.js"></script>
    
    <script>
        $(function(){
            $("html").niceScroll({styler:"fb",cursorcolor:"#4A8BC2", cursorwidth: '8', cursorborderradius: '0px', background: '#404040', cursorborder: '', zindex: '1000'});
            
            $('.price').autoNumeric('init', {aSep: '.', aDec: ',',  mDec: '0'});
        });
    </script>
</body>

</html>
